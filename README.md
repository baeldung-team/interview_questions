***CODE TEST***

This repository contains code test for Baeldung Wordpress Developer Position


**Debug - Code Test**

You are informed that a junior developer was assigned the task of creating a simple function to handle a number of applicants who were expected to submit their names, emails, and photos. An expert database admin setup the database table which meant that all the junior had to worry about was setting up the html form and then developing a function to pass that information to the database.

  
However, upon submitting the code, the Senior Developer is concerned that the code is insecure, buggy, and poorly designed. Considering the urgency of this project the senior has now assigned the task to you, he also encourages you to review the current progress of the junior developers work, and improve on it where you see fit. Also, since the goal is to mentor junior developers, at the end of your task it is expected that you will prepare a brief but informative breakdown and explanation of what the junior developer did wrong and how he can improve in the future.

  
Please review the code very carefully for the mistakes that the Junior developer made.


**Junior Developer HTML Code**

~~~html
<form id="a_time_wasting_gig" method="post" action="#" enctype="multipart/form-data">
    <input type="text" name="name" id="name">
    <input type="email" name="email" id="email">
    <input id="submit" name="submit" type="submit" value="Upload" />
    <input type="file" name="identification_document" id="identification_document"  multiple="false" />
</form>
~~~


**Junior Developer WordPress Code**

~~~php
function doVery_CoolThings(){
    if ( 
        !empty($_POST)
    ) {           
        global $wpdb;
        require_once( ABSPATH . 'wp-admin/includes/image.php' );
        require_once( ABSPATH . 'wp-admin/includes/file.php' );
        require_once( ABSPATH . 'wp-admin/includes/media.php' );
        $d = media_handle_upload( 'identification_document', 0 );
        $r = wp_get_attachment_url($d);
        $wpdb->insert( 
            $wpdb->prefix . 'applicants_table', 
            array( 
                'name' => $_POST['full_name'], 
                'email' => $_POST['email'], 
                'identification_document' => $r
            ) 
        );
        if ( is_wp_error( $d ) ) {
            wp_die( 'Something went horribly wrong. Please try again.' );	
        } 
    } else {
		wp_die( 'Something went horribly wrong. Please try again.' );	
    }
}
~~~
